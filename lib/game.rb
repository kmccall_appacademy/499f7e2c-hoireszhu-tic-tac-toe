require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_accessor :board, :player_one, :player_two, :current_player

  def initialize(player_one, player_two)
    @player_one = player_one
    @player_two = player_two
    @current_player = player_one
    @board = Board.new
  end

  def current_player
    @current_player
  end

  def switch_players!
    if @current_player == player_one
      @current_player = player_two
    else
      @current_player = player_one
    end
  end

  def play_turn
    @current_player.display(board)


    #until board.empty?(next_move)
    #  puts "Error! Enter a valid move"
    next_move = @current_player.get_move
    #end

    @board.place_mark(next_move,@current_player.mark)
    switch_players!
  end

  def play
    until board.over?
      play_turn
    end
    conclude
  end

  def conclude
    puts("Congratulations! You've won!")
  end
end


if __FILE__ == $PROGRAM_NAME
  player_one = ComputerPlayer.new("Bonzo")
  player_two = HumanPlayer.new("Jack")
  test = Game.new
  Game.play
end
