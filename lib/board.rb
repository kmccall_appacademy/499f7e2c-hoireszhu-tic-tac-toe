class Board

  attr_accessor :grid

  def initialize(*grid)
      num_args = grid.size
      if num_args == 0
        @grid = [[nil,nil,nil],[nil,nil,nil],[nil,nil,nil]]
      elsif num_args == 1
        @grid = grid[0]
      end
      @winner = nil
  end

  def place_mark(position,mark)
    if empty?(position)
      grid[position[0]][position[1]] = mark
    else
      raise "error, position is not empty!"
    end
  end

  def empty?(position)
    grid[position[0]][position[1]] == nil
  end

  def winner
    if row_winner[0]
      @winner = row_winner[1]
    elsif column_winner[0]
      @winner = column_winner[1]
    elsif diagonal_winner[0]
      @winner = diagonal_winner[1]
    end
  end

  def over?
    return true if self.winner != nil
    return true if !grid.flatten.include?(nil) && self.winner == nil
    false
  end


  private

  def row_winner
    (0..2).each do |row|
      if grid[row][0]!=nil && grid[row][0]==grid[row][1] && grid[row][0]==grid[row][2]
        return [true,grid[row][0]]
      end
    end
    return [false,nil]
  end

  def column_winner
    (0..2).each do |column|
      if grid[0][column]!=nil && grid[0][column]==grid[1][column] && grid[0][column]==grid[2][column]
        return [true,grid[0][column]]
      end
    end
    return [false,nil]
  end

  def diagonal_winner
    if grid[0][0]!=nil && grid[0][0]==grid[1][1] && grid[2][2]==grid[0][0]
      return [true,grid[0][0]]
    elsif grid[2][0]!=nil && grid[2][0]==grid[1][1] && grid[2][0]==grid[0][2]
      return [true,grid[2][0]]
    end
    return [false,nil]
  end

end
