class ComputerPlayer
  attr_accessor :name, :mark, :board

  def initialize(name)
    @name = name
    @mark = "O"
    @board = Board.new
  end

  def display(current_board)
    puts "new_board\n"
    puts current_board.grid
    @board = current_board
  end

  def get_move
    move = winning_move?
    if move == false
      move = [rand(0..2),rand(0..2)]
      until @board.empty?(move)
        move = [rand(0..2),rand(0..2)]
      end
    end
    move
  end


  private

  def winning_move?
    c_board = @board.grid

    #row_winner
    (0..2).each do |row|
      el1 = c_board[row][0].to_s
      el2 = c_board[row][1].to_s
      el3 = c_board[row][2].to_s
      c_row = [el1,el2,el3]
      if c_row.count("O") == 2 && c_row.include?("")
        return [row,c_row.find_index("")]
      end
    end

    #column_winner
    (0..2).each do |column|
      el1 = c_board[0][column].to_s
      el2 = c_board[1][column].to_s
      el3 = c_board[2][column].to_s
      col = [el1,el2,el3]
      if col.count("O") == 2 && col.include?("")
        puts ("column")
        return [col.find_index(""),column]
      end
    end

    #diagonal_winner
    left_diagonal = [c_board[0][0].to_s,c_board[1][1].to_s,c_board[2][2].to_s]
    if left_diagonal.count("O") == 2 && left_diagonal.include?("")
      idx = left_diagonal.find_index("")
      return [idx,idx]
    end
    right_diagonal = [c_board[0][2].to_s,c_board[1][1].to_s,c_board[2][0].to_s]
    if right_diagonal.count("O") == 2 && right_diagonal.include?("")
      idx = right_diagonal.find_index("")
      case idx
      when 0
        return [0,2]
      when 1
        return [1,1]
      when 2
        return [2,0]
      end
    end

    false
  end

end
