class HumanPlayer
  attr_accessor :name, :mark

  def initialize(name)
    @name = name
    @mark = "X"
  end

  def display(board)
    puts board.grid
  end

  def get_move
    puts "where"
    input = gets
    input.split(",").map(&:to_i)
  end
end
